use std::{
    error,
    fmt::{self, Debug, Display, Formatter},
};

use futures::{
    select, AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt, FutureExt, TryFutureExt,
};
use log::trace;
use u_blox::io::UbloxInterface;

const BUF_SIZE: usize = 8192; // Raspberry Pi I2C doesn't support more anyway

#[derive(Debug)]
pub enum ForwardError<UBX>
where
    UBX: u_blox::io::Error,
{
    Tcp(std::io::Error),
    Ublox(UBX),
}

impl<UBX> Display for ForwardError<UBX>
where
    UBX: u_blox::io::Error + Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Forward ")?;
        match self {
            ForwardError::Tcp(err) => write!(f, "TCP error: {}", err),
            ForwardError::Ublox(err) => write!(f, "U-blox error: {}", err),
        }
    }
}

impl<UBX> error::Error for ForwardError<UBX>
where
    UBX: u_blox::io::Error + error::Error + 'static,
{
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            ForwardError::Tcp(err) => Some(err),
            ForwardError::Ublox(err) => Some(err),
        }
    }
}

pub async fn forward<TCP, UBX>(
    mut stream: TCP,
    mut interface: UBX,
) -> Result<(), ForwardError<UBX::Error>>
where
    TCP: AsyncRead + AsyncWrite + Unpin,
    UBX: UbloxInterface,
{
    let mut tcp_buf = [0u8; BUF_SIZE];
    let mut ubx_buf = [0u8; BUF_SIZE];

    // FIXME: fuse futures outside of loop

    loop {
        select! {
            a = stream.read(&mut tcp_buf).fuse() => {
                let len = a.map_err(ForwardError::Tcp)?;
                if len == 0 {
                    return Ok(());
                }
                trace!("Read from TCP: {len} bytes");
                interface.write(&tcp_buf[..len]).map_err(ForwardError::Ublox).await?;
                trace!("Written to I2C: {len} bytes");
            },
            b = interface.read(&mut ubx_buf).fuse() => {
                let len = b.map_err(ForwardError::Ublox)?;
                if len == 0 {
                    return Ok(());
                }
                trace!("Read from I2C: {len} bytes");
                stream.write_all(&ubx_buf[..len]).map_err(ForwardError::Tcp).await?;
                trace!("Written to TCP: {len} bytes");
            },
        }
    }
}

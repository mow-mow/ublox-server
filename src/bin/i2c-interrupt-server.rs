#![feature(never_type)]

use std::error;

use async_std::{
    net::TcpListener,
    net::{IpAddr, Ipv6Addr, SocketAddr},
};

use embedded_hal::digital::PinState;
use embedded_hal_async::i2c::SevenBitAddress;
use env_logger::Env;
use linux_embedded_hal_async::{digital::LinuxWaitPin, i2c::LinuxI2c};
use log::{error, info};
use u_blox::io::i2c::{UbloxI2c, UbloxInterruptedInterface};

const BIND_ADDR: SocketAddr = SocketAddr::new(IpAddr::V6(Ipv6Addr::UNSPECIFIED), 9069);

const I2C_PATH: &str = "/dev/i2c-1"; // Raspberry Pi I2C bus
const I2C_ADDR: SevenBitAddress = 0x42; // U-blox default I2C address

const PIN_PATH: &str = "/dev/gpiochip0"; // Raspberry Pi GPIO chip
const PIN_NUM: u32 = 27; // Raspberry Pi GPIO 27

async fn get_interface(
) -> Result<UbloxInterruptedInterface<LinuxI2c, LinuxWaitPin>, Box<dyn error::Error>> {
    let i2c = LinuxI2c::new(I2C_PATH)?;
    let pin = LinuxWaitPin::new(PIN_PATH, PIN_NUM)?;

    let mut i2c = UbloxI2c::new(i2c, I2C_ADDR);
    let int = pin;

    i2c.wakeup().await?;
    let interface = UbloxInterruptedInterface::new(i2c, int, PinState::High);

    Ok(interface)
}

#[async_std::main]
async fn main() -> Result<!, Box<dyn error::Error>> {
    let env = Env::new().default_filter_or("info");
    env_logger::init_from_env(env);

    let listener = TcpListener::bind(BIND_ADDR).await?;

    let addr = listener.local_addr().unwrap_or(BIND_ADDR);
    info!("Listening: {addr}");
    loop {
        match listener.accept().await {
            Ok((stream, addr)) => {
                info!("Connection opened: {addr}");
                match get_interface().await {
                    Ok(interface) => {
                        if let Err(err) = ublox_server::forward(stream, interface).await {
                            error!("Forward failed: {err}");
                        }
                    }
                    Err(err) => error!("Interface failed: {err}"),
                }
                info!("Connection closed: {addr}");
            }
            Err(err) => error!("Connection failed: {err}"),
        }
    }
}

#![feature(never_type)]

use std::error;

use async_std::{
    net::TcpListener,
    net::{IpAddr, Ipv6Addr, SocketAddr},
};

use env_logger::Env;
use linux_embedded_hal_async::serial::LinuxSerial;
use log::{error, info};
use u_blox::io::serial::UbloxSerialInterface;

const BIND_ADDR: SocketAddr = SocketAddr::new(IpAddr::V6(Ipv6Addr::UNSPECIFIED), 9069);

const UART_PATH: &str = "/dev/serial0"; // Raspberry Pi UART port
const UART_BAUD: u32 = 38400; // U-Blox default baud rate

async fn get_interface() -> Result<UbloxSerialInterface<LinuxSerial>, Box<dyn error::Error>> {
    let serial = LinuxSerial::new(UART_PATH, UART_BAUD)?;

    let interface = UbloxSerialInterface::new(serial);

    Ok(interface)
}

#[async_std::main]
async fn main() -> Result<!, Box<dyn error::Error>> {
    let env = Env::new().default_filter_or("info");
    env_logger::init_from_env(env);

    let listener = TcpListener::bind(BIND_ADDR).await?;

    let addr = listener.local_addr().unwrap_or(BIND_ADDR);
    info!("Listening: {addr}");
    loop {
        match listener.accept().await {
            Ok((stream, addr)) => {
                info!("Connection opened: {addr}");
                match get_interface().await {
                    Ok(interface) => {
                        if let Err(err) = ublox_server::forward(stream, interface).await {
                            error!("Forward failed: {err}");
                        }
                    }
                    Err(err) => error!("Interface failed: {err}"),
                }
                info!("Connection closed: {addr}");
            }
            Err(err) => error!("Connection failed: {err}"),
        }
    }
}
